﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchPositions : MonoBehaviour
{

    public Transform src;
    public Transform dest;

    // Start is called before the first frame update
    void Start()
    {
        dest.position = src.position;
        dest.rotation = src.rotation;
        dest.localScale = src.localScale;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        dest.position = src.position;
        dest.rotation = src.rotation;
        dest.localScale = src.localScale;

    }
}
