﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashScore : MonoBehaviour
{
    bool _playFireWorks;
    private ParticleSystem _fireworks;
    private Light _light;
    public GameObject prefab;
    // Start is called before the first frame update
    void Start()
    {

        _playFireWorks = false;
        _fireworks = GetComponentInChildren<ParticleSystem>();
        _light= GetComponentInChildren<Light>();
    }

    // Update is called once per frame
    void Update()
    {
    //    if(_playFireWorks)
        if(_fireworks.time > 0.35f)
        {
            _light.enabled = true;
        }
        if(_fireworks.isPlaying == false)
        {
            _light.enabled = false;
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "PaperCrumpled")
        {

            _fireworks.Play();
        }

    }
}
