﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWhenStopped : MonoBehaviour
{
    ParticleSystem _fireworks;
    // Start is called before the first frame update
    void Start()
    {
        _fireworks = GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_fireworks.isPlaying) Destroy(gameObject);
    }
}
