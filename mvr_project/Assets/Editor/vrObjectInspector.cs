﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(vrObject))]
public class vrObjectInspector : Editor
{

    public override void OnInspectorGUI()
    {
        vrObject _vrObject = (vrObject)target;

        _vrObject.isPickable = GUILayout.Toggle(_vrObject.isPickable, "Is Pickable");

        if (_vrObject.isPickable)
        {
            _vrObject.outlineWidth = EditorGUILayout.FloatField("Outline Widht", _vrObject.outlineWidth);
            _vrObject.outlineColor = EditorGUILayout.ColorField("Outline Color", _vrObject.outlineColor);
        }

    }
}
