﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TButt;

public class ClockSetTime : MonoBehaviour, Interactable
{

    [SerializeField] 
    private Clock clock;

    private InteractionController _controller = null;
    private bool _activeInteraction = false;
    private Vector3 _previousControllerPosition;

    private float _minutesAcc = 0;


    private Outline _outline;
    private float _outlineWidth = 10.0f;
    private Color _hoverColor = Color.red;
    private Color _activeColor = Color.green;
    private int _waitingControllers = 0;

    // Start is called before the first frame update
    void Start()
    {
        _outline = gameObject.AddComponent<Outline>();
        _outline.enabled = false;
        _outline.OutlineWidth = _outlineWidth;
        _outline.OutlineColor = _hoverColor;
    }

    // Update is called once per frame
    void Update()
    {
        if (_activeInteraction)
        {
            if (!TBInput.GetButton(TBInput.Button.PrimaryTrigger, _controller.GetController()) || !clock.IsActive())
            {
                Deactivate();
            }
            else
            {
                Vector3 currentControllerPosition = _controller.transform.position;
                Vector2 currentPos = ProjectPosition(currentControllerPosition - transform.position);
                Vector2 prevPos = ProjectPosition(_previousControllerPosition - transform.position);
                float angleDiff = Vector2.SignedAngle(prevPos, currentPos);

                Vector3 rotationEuler = angleDiff * Vector3.forward;
                transform.rotation *= Quaternion.Euler(rotationEuler);
                _previousControllerPosition = currentControllerPosition;
                angleDiff -= 360 * (int)(angleDiff / 360.0f);
                float minutes = angleDiff / 6.0f;
                _minutesAcc += minutes;
                if (Mathf.Abs(_minutesAcc) >= 1)
                {
                    int minDiscrete = (int)(_minutesAcc);
                    _minutesAcc -= minDiscrete;
                    clock.MakeStepTimeMinutes(minDiscrete);
                }
            }
        }
    }

    private void Deactivate()
    {
        _controller.Deactivate(gameObject.GetInstanceID());
        if (_waitingControllers > 0)
        {
            _outline.OutlineColor = _hoverColor;
        }
        else
        {
            _outline.enabled = false;
        }
        _activeInteraction = false;
        _controller = null;
        clock.SetRunning(true);
    }

    Vector2 ProjectPosition(Vector3 pos)
    {
        return new Vector2(Vector3.Dot(pos, transform.right), Vector3.Dot(pos, transform.up));
    }

    public bool IsActive(InteractionController controller)
    {
        return _activeInteraction && controller.GetInstanceID() == _controller.GetInstanceID();
    }
    public bool IsActive()
    {
        return _activeInteraction;
    }

    public bool TryToActivate(InteractionController controller)
    {
        if (!_activeInteraction && clock.IsActive() && !clock.IsActive(controller) && TBInput.GetButton(TBInput.Button.PrimaryTrigger, controller.GetController()))
        {
            _outline.OutlineColor = _activeColor;
            _activeInteraction = true;
            _controller = controller;
            _previousControllerPosition = _controller.transform.position;
            _minutesAcc = 0;
            clock.SetRunning(false);
            return true;
        }

        return false;
    }

    public void OnWaitToActivateStart(InteractionController controller)
    {
        _waitingControllers++;
        if (!_activeInteraction)
        {
            _outline.enabled = true;
            _outline.OutlineColor = _hoverColor;
        }
    }
    public void OnWaitToActivateEnd(InteractionController controller)
    {
        _waitingControllers--;
        if (!_activeInteraction && _waitingControllers < 1)
        {
            _outline.enabled = false;
        }
    }
}
