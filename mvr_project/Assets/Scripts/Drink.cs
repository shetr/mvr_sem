﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TButt;
using UnityEngine.XR;

public class Drink : MonoBehaviour
{
    private static float defaultTimer = 1.0f;
    private float timer = defaultTimer;

    public Material fullBottle;
    public Material emptyBottle;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material = fullBottle;

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Mathf.Round(gameObject.transform.rotation.eulerAngles.x) + " " + Mathf.Round(gameObject.transform.rotation.eulerAngles.y) + " " + Mathf.Round(gameObject.transform.rotation.eulerAngles.z));
        //Debug.Log(Mathf.Round(gameObject.transform.rotation.eulerAngles.z));

        //if z>270 || z<-90
        //if x>270 || x<-90
        float x = gameObject.transform.rotation.eulerAngles.x;
        float z = gameObject.transform.rotation.eulerAngles.z;
        
        float dist = Vector3.Distance(Camera.main.transform.position, gameObject.transform.position);
       // Debug.Log("dist: " + dist + " z,x: " + Mathf.Round(x) + " " + Mathf.Round(z));
        
        if (dist < 0.3f)
        {
            z += 360;
            x += 360;
            if (z > 300 || z < 420 || x > 300 || x < 420)
            {
                timer -= Time.deltaTime;
                //actually start playing sound here and finish when timer < 0
            }
            //Debug.Log(timer);
            if (timer <= 0)
            {
                //Play sound here
                GetComponent<Renderer>().material = emptyBottle;

            }
        }
    }
}
