﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paper : MonoBehaviour
{

    private Texture2D tex;
    // Start is called before the first frame update
    void Start()
    {
        var mf = GetComponent<MeshFilter>();
        Mesh mesh = null;
        if (mf != null)
            mesh = mf.mesh;

        if (mesh == null || mesh.uv.Length != 24)
        {
            //  Debug.Log("Script needs to be attached to built-in cube");
            //  return;
        }

        var uvs = mesh.uv;

        //// Top
        //uvs[8] = Vector2(0.334, 0.0);
        //uvs[9] = Vector2(0.666, 0.0);
        //uvs[4] = Vector2(0.334, 0.333);
        //uvs[5] = Vector2(0.666, 0.333);


        // Bottom
        uvs[12] = new Vector2(0.0f, 1.0f);
        uvs[14] = new Vector2(1.0f, 0.0f);
        uvs[15] = new Vector2(1.0f, 1.0f);
        uvs[13] = new Vector2(0.0f, 0.0f);
        mesh.uv = uvs;
        var c = transform.parent.GetComponentInChildren<MeshCollider>();
        c.sharedMesh.uv = uvs;


        Renderer ren = transform.GetComponent<Renderer>();
        tex = ren?.material.mainTexture as Texture2D;
        Texture2D newTex = new Texture2D(tex.width, tex.height, tex.format,true);
        Graphics.CopyTexture(tex, newTex);
        if(ren) ren.material.mainTexture = newTex;


       
  


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
