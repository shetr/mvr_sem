﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class DigitalClock : MonoBehaviour
{

    private String prevString = "";

    // Start is called before the first frame update
    void Start()
    {
        
    }


    // Update is called once per frame
    void Update()
    {
        DateTime time = DateTime.Now;
        String newString = TimeToString(time);
        if (!newString.Equals(prevString))
        {
            TextMeshPro textMesh = gameObject.GetComponent<TextMeshPro>();

            textMesh.text = TimeToString(time);
            prevString = newString;
        }
    }

    string TimeToString(DateTime time)
    {
        string text = "";
        if (time.Hour < 10)
        {
            text += "0";
        }
        text += time.Hour.ToString() + ":";
        if (time.Minute < 10)
        {
            text += "0";
        }
        text += time.Minute.ToString() + ":";
        if (time.Second < 10)
        {
            text += "0";
        }
        text += time.Second.ToString();
        return text;
    }
}
