﻿using System;
using System.Collections;
using System.Collections.Generic;
using TButt;
using UnityEngine;

public class ControllerGrab : MonoBehaviour
{
    public GameObject crumbpledPaperPrefab;
    public ControllerGrab otherController;
    public float distanceToCrumblePaper = 0.1f;
    public bool debug = false;
    public float penRot;
    public Vector3 penTransform;
    private bool _canCrumple = true;

    [HideInInspector]
    public GameObject _activeGameObject = null;

    public TBInput.Controller controller;


    public bool _grabbing = false;
    private Transform _lastParent;
    private Vector3 lastPos;
    [SerializeField]
    private float _throwStrength = 10.0f;
    [SerializeField]
    private float _distanceIgnore = 0.01f;

    // Start is called before the first frame update
    void Start()
    {
        lastPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        handleGrab();
        crumplePaper();
        lastPos = transform.position;
        if(debug)
        if (_activeGameObject != null)
        {
            if (_activeGameObject.tag == "Pencil")
            {
                _activeGameObject.transform.rotation = transform.rotation;
                _activeGameObject.transform.Rotate(penRot, 0.0f, 0.0f);
                _activeGameObject.transform.position = transform.position;
                _activeGameObject.transform.Translate(penTransform * 0.1f);
                }
            }
    }

    private void crumplePaper()
    {
        if (Vector3.Distance(otherController.transform.position, this.transform.position) > distanceToCrumblePaper + 0.1f)
        {
            _canCrumple = true;
            return;
        }
        if(_grabbing && _canCrumple)
        {
            var nextCrumplePaper = _activeGameObject.GetComponent<NextCrumplePaper>();
            if(nextCrumplePaper != null)
            {
                if (Vector3.Distance(otherController.transform.position, this.transform.position) < distanceToCrumblePaper)
                {
                    var objectToDestroy = _activeGameObject;
                    _activeGameObject = Instantiate(nextCrumplePaper.paperPrefab, transform.position, transform.rotation);
                    //_activeGameObject.GetComponent<Outline>().enabled = false;
                    _activeGameObject.GetComponent<Rigidbody>().isKinematic = true;
                    _lastParent = _activeGameObject.transform.parent;
                    _activeGameObject.transform.parent = this.transform;
                    _activeGameObject.GetComponent<Renderer>().material = objectToDestroy.GetComponentInChildren<Renderer>().material;
                    Destroy(objectToDestroy);
                    _canCrumple = false;
                }
            }
        }
    }

    private void handleGrab()
    {

        if (_activeGameObject != null)
        {
            Rigidbody rigidBody = _activeGameObject.transform.GetComponentInChildren<Rigidbody>();


            if (TBInput.GetButtonDown(TBInput.Button.PrimaryTrigger, controller)) {
                _activeGameObject.GetComponent<Outline>().enabled = false;
                 rigidBody.isKinematic = true;
                 _lastParent = _activeGameObject.transform.parent;
                _activeGameObject.transform.parent = this.transform;
                _grabbing = true;

                if (_activeGameObject.tag == "Pencil")
                {
                    _activeGameObject.transform.rotation = transform.rotation;
                    _activeGameObject.transform.Rotate(250.0f, 0.0f, 0.0f);
                    
                    _activeGameObject.transform.position = transform.position;
                    //_activeGameObject.transform.Translate(0.0f, 0.0f, -0.05f);
                    _activeGameObject.GetComponent<Rigidbody>().detectCollisions = false;
                }
            }
            else if (TBInput.GetButtonUp(TBInput.Button.PrimaryTrigger, controller))
            {
                _activeGameObject.GetComponent<Outline>().enabled = true;
                rigidBody.isKinematic = false;
                 _activeGameObject.transform.parent = _lastParent;
                if (Vector3.Distance(transform.position, lastPos) > _distanceIgnore)
                {

                    Vector3 vel = _throwStrength * (this.transform.position - lastPos) / Time.deltaTime;
                    rigidBody.velocity = vel;
                }
                _grabbing = false;
                
                if (_activeGameObject.tag == "Pencil") _activeGameObject.GetComponent<Rigidbody>().detectCollisions = true;
            }

            

        }

    }


    private void OnTriggerEnter(Collider other)
    {
        var vr = other.gameObject.GetComponent<vrObject>();
        if (!_activeGameObject && ((vr != null && vr.isPickable) || other.gameObject.tag == "PaperCube"))
        {
            _activeGameObject = other.gameObject;
            if (_activeGameObject.tag == "PaperCube")
            {
                _activeGameObject = _activeGameObject.transform.parent.gameObject;
            }
            _activeGameObject.GetComponent<Outline>().enabled = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!_grabbing && _activeGameObject)
        {
 
            _activeGameObject.GetComponent<Outline>().enabled = false;
            _activeGameObject = null;
            
        }
    }

    private void OnTriggerStay(Collider other)
    {

        var vr = other.gameObject.GetComponent<vrObject>();
        if (!_activeGameObject && ((vr != null && vr.isPickable) || other.gameObject.tag == "PaperCube"))
        {
            _activeGameObject = other.gameObject;
            if (_activeGameObject.tag == "PaperCube") _activeGameObject = _activeGameObject.transform.parent.gameObject;
            _activeGameObject.GetComponent<Outline>().enabled = true;
        }
    }

}
