﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorRotation : MonoBehaviour
{

    [SerializeField]
    private float minAngle = 0;
    [SerializeField]
    private float maxAngle = 90;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float GetRotation()
    {
        return transform.localRotation.eulerAngles.y;
    }

    public bool SetRotation(float angle)
    { 
        if(angle < minAngle || angle > maxAngle)
        {
            return false;
        }

        transform.localRotation = Quaternion.Euler(transform.up * angle);

        return true;
    }
}
