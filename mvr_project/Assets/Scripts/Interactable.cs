﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Interactable
{
    bool TryToActivate(InteractionController controller);
    bool IsActive(InteractionController controller);
    bool IsActive();

    void OnWaitToActivateStart(InteractionController controller);
    void OnWaitToActivateEnd(InteractionController controller);
}
