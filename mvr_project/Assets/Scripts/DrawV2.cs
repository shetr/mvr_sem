﻿ using System.Collections;
using System.Collections.Generic;
using TButt;
using UnityEngine;

public class DrawV2 : MonoBehaviour
{
    public ControllerGrab _controllerGrab;
    public TBInput.Controller _controller;
    public GameObject _brush;
    public float _brushSize = 0.01f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (_controllerGrab._grabbing)
        {

            var forward = _controllerGrab._activeGameObject.transform.TransformDirection(Vector3.forward) * -5;
            Debug.DrawRay(_controllerGrab._activeGameObject.transform.position, forward, Color.green);

            RaycastHit[] hits;

            LayerMask mask = LayerMask.GetMask("Paper");
            hits = Physics.RaycastAll(_controllerGrab._activeGameObject.transform.position, forward, 10.0f, mask);
            if (hits.Length > 0)
            {
                RaycastHit hit = hits[0];
                if (hit.collider.gameObject.layer == 9)
                {

                    var renderer = hit.transform.GetComponent<Renderer>();
                    //var meshCollider = hit.collider as MeshCollider;
                    if (!renderer || !renderer.sharedMaterial || !renderer.sharedMaterial.mainTexture)
                    {
                        Debug.Log("returned");
                        return;
                    }

                    var go = Instantiate(_brush, hit.point + hit.normal * 0.001f, Quaternion.identity, hit.transform);
                    go.transform.localScale = Vector3.one * _brushSize;
                    
                }
            }
        }

    }
}
