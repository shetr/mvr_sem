﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : MonoBehaviour, Interactable
{

    [SerializeField]
    private Material lightOn;
    [SerializeField]
    private Material lightOff;
    [SerializeField]
    private GameObject light;
    [SerializeField]
    private GameObject lightBulb;

    private bool _isOn = true;

    private Vector3 _distanceThreshold = new Vector3(0.15f, 0.15f, 0.15f);
    private Vector3 _firstDistance = Vector3.zero;
    private InteractionController _controller = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_controller != null)
        {
            Vector3 distance = GetDistance(_controller.transform.position, transform.position);
            if(IsOutOfBound(distance))
            {
                _controller = null;
            }
        }
        
    }

    bool IsOutOfBound(Vector3 v)
    {
        return v.x > _distanceThreshold.x || v.y > _distanceThreshold.y || v.z > _distanceThreshold.z;
    }

    Vector3 GetDistance(Vector3 pos1, Vector3 pos2)
    {
        return new Vector3(Mathf.Abs(pos1.x - pos2.x), Mathf.Abs(pos1.y - pos2.y), Mathf.Abs(pos1.z - pos2.z));
    }


    public bool IsActive(InteractionController controller)
    {
        return false;
    }

    public bool IsActive()
    {
        return false;
    }

    public void OnWaitToActivateEnd(InteractionController controller)
    {
        
    }

    public void OnWaitToActivateStart(InteractionController controller)
    {
        if(_controller == null)
        {
            _isOn = !_isOn;
            if (_isOn)
            {
                transform.localRotation = Quaternion.Euler(Vector3.forward * 15);
                lightBulb.GetComponent<Renderer>().material = lightOn;
                light.GetComponent<Light>().enabled = true;
            }
            else
            {
                transform.localRotation = Quaternion.Euler(Vector3.forward * -15);
                lightBulb.GetComponent<Renderer>().material = lightOff;
                light.GetComponent<Light>().enabled = false;
            }
            _controller = controller;
            _firstDistance = GetDistance(controller.transform.position, transform.position);
        }
    }

    public bool TryToActivate(InteractionController controller)
    {

        return false;
    }
}
