﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TButt;

public class Pee : MonoBehaviour
{

    public float peeAmount;
    public bool peeing;
    private InteractionController _controller = null;
    public ParticleSystem peeParticles;

    public Material fullBottle;
    public Material emptyBottle;
    public GameObject Bottle;
    public GameObject peeOrigin;

    private Vector3 prevLocation;

    // Start is called before the first frame update
    void Start()
    {
        peeAmount = 0.0f;
        peeing = false;
        peeParticles.Pause();
        gameObject.transform.position = peeOrigin.transform.position;
        prevLocation = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(Camera.main.transform.position, gameObject.transform.position);
        //Debug.Log("dist: " + dist);
        if (dist < 1.5f)
        {
            GetComponent<Renderer>().enabled = true;
            //Debug.Log(Bottle.GetComponent<Renderer>().material.name + " ?? " + emptyBottle.name);
            //empty bottle (instance) ?? empty bottle

            if (Bottle.GetComponent<Renderer>().material.name.Contains(emptyBottle.name))
            {
                peeAmount += 10.0f;
                Bottle.GetComponent<Renderer>().material = fullBottle;
            }
            
            if ( TBInput.GetButtonDown(TBInput.Button.Touchpad, TBInput.Controller.RHandController) || 
                TBInput.GetButtonDown(TBInput.Button.Touchpad, TBInput.Controller.LHandController))
            {
                peeing = true;
                if(peeAmount > 0)
                {
                    peeParticles.Play();
                }

            }
            if (TBInput.GetButtonUp(TBInput.Button.Touchpad, TBInput.Controller.RHandController) ||
               TBInput.GetButtonUp(TBInput.Button.Touchpad, TBInput.Controller.LHandController))
            {
                peeing = false;
                peeParticles.Stop();
            }
            if (peeing)
            {
                peeAmount -= 0.05f; //KEKL - vázaný na framy
            }
        }
        else
        {
            GetComponent<Renderer>().enabled = false;
        }
        
        float distFromOrigin = Vector3.Distance(peeOrigin.transform.position, gameObject.transform.position);
        if(distFromOrigin >= 0.5f)
        {
            gameObject.transform.position = prevLocation;
        }
        prevLocation = gameObject.transform.position;
    }
}
