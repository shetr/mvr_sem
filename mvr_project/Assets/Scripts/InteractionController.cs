﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TButt;

public class InteractionController : MonoBehaviour
{
    [SerializeField] private TBInput.Controller controller;

    private Dictionary<int, GameObject> interactables = new Dictionary<int, GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public TBInput.Controller GetController() { return controller; }

    public bool HasInteraction()
    {
        return interactables.Count > 0;
    }

    void CheckCollision(Collider other)
    {
        Interactable interactable = other.gameObject.GetComponent<Interactable>();
        if (interactable != null)
        {
            if (!interactable.IsActive(this))
            {
                if (interactable.TryToActivate(this))
                {
                    interactables.Add(other.gameObject.GetInstanceID(), other.gameObject);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Interactable interactable = other.gameObject.GetComponent<Interactable>();
        if (interactable != null)
        {
            interactable.OnWaitToActivateStart(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Interactable interactable = other.gameObject.GetComponent<Interactable>();
        if (interactable != null)
        {
            interactable.OnWaitToActivateEnd(this);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        CheckCollision(other);
    }

    public void Deactivate(int id)
    {
        interactables.Remove(id);
    }
}
