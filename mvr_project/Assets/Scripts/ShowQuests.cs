﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TButt;
using UnityEngine.XR;

public class ShowQuests : MonoBehaviour
{
    private static float defaultTimer = 1.0f;
    private float timer = defaultTimer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Mathf.Round(gameObject.transform.rotation.eulerAngles.x) + " " + Mathf.Round(gameObject.transform.rotation.eulerAngles.y) + " " + Mathf.Round(gameObject.transform.rotation.eulerAngles.z));
        //Debug.Log(Mathf.Round(gameObject.transform.rotation.eulerAngles.z));

        //0..360 - facing top Z around 0

        if (gameObject.transform.rotation.eulerAngles.z > 330 || gameObject.transform.rotation.eulerAngles.z < 30)
        {
            timer -= Time.deltaTime;
            //Debug.Log(timer);
            if (timer <= 0)
            {
                gameObject.GetComponent<Renderer>().enabled = true;
            }
        }
        else
        {
            timer = defaultTimer;
            gameObject.GetComponent<Renderer>().enabled = false;
        }

    }
}
