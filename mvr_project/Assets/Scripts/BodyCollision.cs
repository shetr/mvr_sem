﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyCollision : MonoBehaviour
{
    private GameObject VRcam;
    // Start is called before the first frame update
    void Start()
    {
        VRcam = GameObject.Find("Main Camera/Tracking Volume/Standard VR Camera");
        gameObject.transform.position = VRcam.transform.position;
        gameObject.transform.SetParent(VRcam.transform);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
