﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vrObject : MonoBehaviour
{
    public bool isPickable = true;
    public float outlineWidth = 5.0f;
    public Color outlineColor = Color.red;
    private Outline outline;

    // Start is called before the first frame update
    void Start()
    {
        if (isPickable)
        {
            outline = gameObject.AddComponent<Outline>();
            outline.enabled = false;
            outline.OutlineWidth = outlineWidth;
            outline.OutlineColor = outlineColor;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
