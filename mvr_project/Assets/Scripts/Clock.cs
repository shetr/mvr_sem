﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TButt;

public class Clock : MonoBehaviour, Interactable
{

    [SerializeField]
    private Transform hoursPivot;
    [SerializeField]
    private Transform minutesPivot;
    [SerializeField]
    private Transform secondsPivot;

    [SerializeField]
    bool initRandom = true;

    [SerializeField, Range(0, 11)]
    private int hour = 0;
    [SerializeField, Range(0, 59)]
    private int minute = 0;
    [SerializeField, Range(0, 60)]
    private float second = 0;

    [SerializeField]
    bool hoursDiscrete = false;
    [SerializeField]
    bool minutesDiscrete = true;
    [SerializeField]
    bool secondsDiscrete = true;

    private bool _isRunning = true;

    private InteractionController _controller = null;
    private bool _activeInteraction = false;
    private Transform _lastParent = null;


    private Outline _outline;
    private float _outlineWidth = 10.0f;
    private Color _outlineColor = Color.red;

    private int _waitingControllers = 0;

    private bool _onWall = false;
    private Rigidbody _rigidbody;
    private Collider _collider;
    private Vector3[] _wallDirs;
    private float[] _wallAngles;
    private Vector3 _lastWallHitPos = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        _wallDirs = new Vector3[4] {
            new Vector3(1,0,0),
            new Vector3(-1,0,0),
            new Vector3(0,0,1),
            new Vector3(0,0,-1)
        };
        _wallAngles = new float[4] {
            -90,
            90,
            180,
            0
        };
        _outline = gameObject.AddComponent<Outline>();
        _rigidbody = GetComponent<Rigidbody>();
        _collider = GetComponent<Collider>();
        _outline.enabled = false;
        _outline.OutlineWidth = _outlineWidth;
        _outline.OutlineColor = _outlineColor;

        if (initRandom)
        {
            second = Random.Range(0, 60);
            minute = Random.Range(0, 60);
            hour = Random.Range(0, 12);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_activeInteraction)
        {
            HandleGrab();
        }
        if(_isRunning)
        {
            MakeStepTime(Time.deltaTime);
        }
        PreserveConsistency();
        UpdatePivots();
    }


    void HandleGrab()
    {
        if (!TBInput.GetButton(TBInput.Button.PrimaryTrigger, _controller.GetController()) && _isRunning)
        {
            Deactivate();
        }
    }

    private void Deactivate()
    {
        if (_waitingControllers > 0)
        {
            _outline.enabled = true;
        }
        _controller.Deactivate(gameObject.GetInstanceID());
        _activeInteraction = false;
        _controller = null;
        this.transform.parent = _lastParent;
        if (_onWall)
        {
            _rigidbody.useGravity = false;
            _collider.isTrigger = true;
            transform.localPosition = _lastWallHitPos;
        }
        else
        {
            _rigidbody.useGravity = true;
            _collider.isTrigger = false;
        }
    }

    public void MakeStepTimeMinutes(int minutes)
    {
        if (minutes >= 0)
        {
            minute += minutes;
            hour += minute / 60;
            PreserveConsistency();
        }
        else
        {
            minute += minutes;
            if (minute < 0)
            {
                hour += (minute / 60) - 1;
                minute %= 60;
                minute += 60;
                if (hour < 0)
                {
                    hour %= 12;
                    hour += 12;
                }
            }
        }
    }

    void MakeStepTime(float deltaTime)
    {
        if (deltaTime >= 0)
        {
            second += deltaTime;
            minute += (int)(second / 60.0f);
            hour += minute / 60;
        }
        else
        {
            second += deltaTime;
            if(second < 0)
            {
                minute += (int)(second / 60.0f) - 1;
                second -= (int)(second / 60.0f) * 60;
                second += 60;
                if (minute < 0) 
                {
                    hour += (minute / 60) - 1;
                    minute %= 60;
                    minute += 60;
                    if (hour < 0)
                    {
                        hour %= 12;
                        hour += 12;
                    }
                }
            }
        }
    }

    void PreserveConsistency()
    {
        second -= (int)(second / 60.0f) * 60;
        minute %= 60;
        hour %= 12;
    }

    void UpdatePivots()
    {
        float secondVal = secondsDiscrete ? (int)second : second;
        float minuteVal = minutesDiscrete ? minute : minute + second / 60.0f;
        float hourVal = hoursDiscrete ? hour : hour + (minute + second / 60.0f) / 60.0f;

        float hourNorm = hourVal / 12.0f;
        float minuteNorm = minuteVal / 60.0f;
        float secondNorm = secondVal / 60.0f;

        float hoursAngle = 360.0f * (hourNorm - (int)hourNorm);
        float minutesAngle = 360.0f * (minuteNorm - (int)minuteNorm);
        float secondsAngle = 360.0f * (secondNorm - (int)secondNorm);

        hoursPivot.localRotation = Quaternion.Euler(0, 0, hoursAngle);
        minutesPivot.localRotation = Quaternion.Euler(0, 0, minutesAngle);
        secondsPivot.localRotation = Quaternion.Euler(0, 0, secondsAngle);
    }

    public bool TryToActivate(InteractionController controller)
    {
        if (!_activeInteraction && TBInput.GetButton(TBInput.Button.PrimaryTrigger, controller.GetController()))
        {
            _outline.enabled = false;
            _controller = controller;
            _activeInteraction = true;
            _lastParent = transform.parent;
            this.transform.parent = _controller.transform;
            _rigidbody.useGravity = false;
            _collider.isTrigger = true;
            return true;
        }

        return false;
    }

    public void SetRunning(bool isRunning) { _isRunning = isRunning; }

    public bool IsActive(InteractionController controller)
    {
        return _activeInteraction && controller.GetInstanceID() == _controller.GetInstanceID();
    }
    public bool IsActive()
    {
        return _activeInteraction;
    }


    public void OnWaitToActivateStart(InteractionController controller)
    {
        _waitingControllers++;
        if (!_activeInteraction)
        {
            _outline.enabled = true;
        }
    }
    public void OnWaitToActivateEnd(InteractionController controller)
    {
        _waitingControllers--;
        if(_waitingControllers < 1)
        {
            _outline.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Walls"))
        {
            _onWall = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Walls"))
        {
            _onWall = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Walls"))
        {
            if(_activeInteraction)
            {
                int collDir = GetWallCollisionDir();
                if (collDir != -1)
                {
                    transform.rotation = Quaternion.Euler(0, _wallAngles[collDir], 0);
                    if (Vector3.Distance(transform.position + transform.forward, _lastWallHitPos) < 1)
                    {
                        Vector3 dirToHit = _lastWallHitPos - transform.position;
                        Vector3 dirPerpendicularToWall = Vector3.Dot(dirToHit, _wallDirs[collDir]) * _wallDirs[collDir];
                        transform.position += dirPerpendicularToWall;
                    }
                }
            }
        }
    }

    private int GetWallCollisionDir()
    {
        int layer_mask = LayerMask.GetMask("Walls");
        float dist = float.MaxValue;
        int resDir = -1;
        for(int i = 0; i < _wallDirs.Length; i++)
        {
            Vector3 dir = _wallDirs[i];
            RaycastHit hit;
            if (Physics.Raycast(transform.position + transform.forward, dir, out hit, float.MaxValue, layer_mask))
            {
                if(hit.distance < dist)
                {
                    dist = hit.distance;
                    resDir = i;
                    _lastWallHitPos = hit.point;
                }
            }
        }


        return resDir;
    }
}
