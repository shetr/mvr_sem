﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TButt;

public class DoorHandle : MonoBehaviour, Interactable
{

    [SerializeField]
    private DoorRotation doorRotaion;
    [SerializeField]
    private DoorHandle otherHandle;


    private InteractionController _controller = null;
    private bool _activeInteraction = false;
    private Vector3 _startControllerPosition;
    private Vector2 _startControllerProjectedPosition;
    private float _startRotation;

    private Outline _outline;
    private float _outlineWidth = 5.0f;
    private Color _hoverColor = Color.red;
    private Color _activeColor = Color.green;
    private Color _limitColor = Color.yellow;
    private int _waitingControllers = 0;

    // Start is called before the first frame update
    void Start()
    {
        _outline = gameObject.AddComponent<Outline>();
        _outline.enabled = false;
        _outline.OutlineWidth = _outlineWidth;
        _outline.OutlineColor = _hoverColor;
    }

    // Update is called once per frame
    void Update()
    {
        if (_activeInteraction)
        {
            if (!TBInput.GetButton(TBInput.Button.PrimaryTrigger, _controller.GetController()))
            {
                _controller.Deactivate(gameObject.GetInstanceID());
                if (_waitingControllers > 0)
                {
                    _outline.OutlineColor = _hoverColor;
                }
                else
                {
                    _outline.enabled = false;
                }
                _activeInteraction = false;
                _controller = null;
            }
            else
            {
                Vector3 currentControllerPosition = _controller.transform.position;
                Vector2 projectedCurrent = ProjectPos(currentControllerPosition - doorRotaion.transform.position);
                float angle = Vector2.SignedAngle(_startControllerProjectedPosition, projectedCurrent);
                bool isSet = doorRotaion.SetRotation(_startRotation + angle);
                if(isSet)
                {
                    _outline.OutlineColor = _activeColor;
                }
                else
                {
                    _outline.OutlineColor = _limitColor;
                }
            }
        }
    }

    Vector2 ProjectPos(Vector3 pos)
    { 
        return new Vector2(Vector3.Dot(pos, Vector3.forward), Vector3.Dot(pos, Vector3.right));
    }

    public bool IsActive(InteractionController controller)
    {
        return _activeInteraction && controller.GetInstanceID() == _controller.GetInstanceID();
    }

    public bool IsActive()
    {
        return _activeInteraction;
    }

    public void OnWaitToActivateEnd(InteractionController controller)
    {
        _waitingControllers--;
        if (!_activeInteraction && _waitingControllers < 1)
        {
            _outline.enabled = false;
        }
    }

    public void OnWaitToActivateStart(InteractionController controller)
    {
        _waitingControllers++;
        if (!_activeInteraction)
        {
            _outline.enabled = true;
            _outline.OutlineColor = _hoverColor;
        }
    }

    public bool TryToActivate(InteractionController controller)
    {
        if (!_activeInteraction && TBInput.GetButton(TBInput.Button.PrimaryTrigger, controller.GetController()) && !controller.HasInteraction() && !otherHandle.IsActive())
        {
            _outline.OutlineColor = _activeColor;
            _activeInteraction = true;
            _controller = controller;
            _startControllerPosition = _controller.transform.position;
            _startControllerProjectedPosition = ProjectPos(_startControllerPosition - doorRotaion.transform.position);
            _startRotation = doorRotaion.GetRotation();
            return true;
        }

        return false;
    }
}
