﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TButt;
using System;

public class Draw : MonoBehaviour
{

    public ControllerGrab _controllerGrab;
    public TBInput.Controller _controller;
    public int _width = 16;
    public int _height = 16;
    public float _sensitivityPixelDrawDistance = 16.0f;
    private Color32[] _colors;
    private Vector2 _lastPixelUV = new Vector2(-1, -1);
    void Start()
    {
        _colors = getColorArray(_width, _height);
    }

    private Color32[] getColorArray(int w, int h)
    {
        Color32[] c = new Color32[w * h];
        for (int i = 0; i < w * h; i++)
        {
            c[i] = Color.black;
        }
        return c;
    }

    private void Awake()
    {
        
    }

    void FixedUpdate()
    {

    }




    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != 30) return;

        if (_controllerGrab._grabbing && _controllerGrab._activeGameObject == transform.parent.gameObject)
        {
            _lastPixelUV = new Vector2(-1, -1);
        }

    }

    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.layer != 30) return;
        if (_controllerGrab._grabbing && _controllerGrab._activeGameObject == transform.parent.gameObject)
        {
            var writerPos = transform.GetChild(0);
            var forward = writerPos.TransformDirection(Vector3.forward) * -5;
            

            RaycastHit[] hits;

            LayerMask mask = LayerMask.GetMask("PaperMesh");
            hits = Physics.RaycastAll(_controllerGrab._activeGameObject.transform.position, forward, 10.0f, mask);
            if (hits.Length > 0 )
            {
                RaycastHit hit = hits[0];
                if (Vector3.Angle(hit.normal, forward) < 90.0f) return;
                foreach(var _hit in hits)
                {
                    if (_hit.collider.GetType() == typeof(MeshCollider) &&  _hit.collider.gameObject.layer == 30) hit = _hit;
                }
                if (hit.collider.gameObject.layer == 30)
                {
                    var renderer = hit.transform.parent.GetComponentInChildren<Renderer>();
                    if (!renderer || !renderer.sharedMaterial || !renderer.sharedMaterial.mainTexture)
                    {
                        return;
                    }

                    Texture2D tex = renderer.material.mainTexture as Texture2D;
                    Vector2 pixelUV = hit.textureCoord;

                    pixelUV.x *= tex.width;
                    pixelUV.y *= tex.height;
                    if (_lastPixelUV == new Vector2(-1, -1))
                        try
                        {
                            int w = _width < tex.width ? _width : tex.width;
                            int h = _height < tex.height ? _height : tex.height;
                            Color32[] c;//; = _colors;
                            c = getColorArray(w, h);
                            tex.SetPixels32((int)pixelUV.x, (int)pixelUV.y, w, h, c);
                        }
                        catch(Exception e)
                        {
                            return;
                        }
                    else if (sensitivityCheck(pixelUV)) return;
                    else
                    {
                        DrawLine(tex, _lastPixelUV, pixelUV);
                    }
                    TBInput.SetRumble(_controller, 0.001f);
                    _lastPixelUV = pixelUV;
                    tex.Apply();

                }
            }
        }
    }

    private bool sensitivityCheck(Vector2 pixelUV)
    {
        return Vector2.Distance(pixelUV, _lastPixelUV) < _sensitivityPixelDrawDistance;
    }

    private void DrawLine(Texture2D tex, Vector2 lastPixelUV, Vector2 pixelUV)
    {
        Vector2 t = lastPixelUV;
        float frac = 1 / Mathf.Sqrt(Mathf.Pow(pixelUV.x - lastPixelUV.x, 2) + Mathf.Pow(pixelUV.y - lastPixelUV.y, 2));
        float ctr = 0;

        while ((int)t.x != (int)pixelUV.x || (int)t.y != (int)pixelUV.y)
        {
            t = Vector2.Lerp(lastPixelUV, pixelUV, ctr);
            ctr += frac;
            try
            {
                int w = _width < tex.width ? _width : tex.width;
                int h = _height < tex.height? _height : tex.height;
                Color32[] c;//; = _colors;
                c = getColorArray(w, h);
                tex.SetPixels32((int)t.x, (int)t.y, w, h, c);

            }
            catch(Exception e)
            {
                return;
            }
        }
    }
}
