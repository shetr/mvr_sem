﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TButt;
using System.Diagnostics;

public class TeleportController : MonoBehaviour
{
    [SerializeField] private TBInput.Controller controller;
    [SerializeField] private GameObject teleportationRingPrefab;
    [SerializeField] private float floorPosition = 1;
    [SerializeField] private float lineWidth = 0.1f;
    [SerializeField] private Color lineColor = new Color(0f, 0.68773f, 1f);
    [SerializeField] private LineRenderer lineRenderer = null;

    private GameObject _teleportationRing = null;
    private bool pressed = false;

    // Start is called before the first frame update
    void Start()
    {


        lineRenderer.startColor = lineColor;
        lineRenderer.endColor = lineColor;
        lineRenderer.startWidth = lineWidth;
        lineRenderer.endWidth = lineWidth;
        lineRenderer.positionCount = 0;

    }

    // Update is called once per frame
    void Update()
    {
        int isLeft = controller == TBInput.Controller.LHandController ? 1 : -1;
        //Vector3 rayDir = /*Quaternion.AngleAxis(45, transform.right) * Quaternion.AngleAxis(-10, transform.up) **/ transform.forward;
        Vector3 rayDir = Quaternion.FromToRotation(transform.forward, (isLeft * 2 * transform.right - 3 * transform.up + 5 * transform.forward).normalized) * transform.forward;

        RaycastHit hit;


        if (Physics.Raycast(transform.position, rayDir, out hit, float.MaxValue)) {
            Vector3 hitPos = hit.point;

            if (hitPos.y == floorPosition) { 
                if (TBInput.GetButtonDown(TBInput.Button.SecondaryTrigger, controller))
                {
                    _teleportationRing = Instantiate(teleportationRingPrefab, hitPos, Quaternion.identity);
                }
                else if (TBInput.GetButtonUp(TBInput.Button.SecondaryTrigger, controller))
                {
                    TBCameraRig.instance.transform.position = hitPos;
                    Destroy(_teleportationRing);
                    _teleportationRing = null;
                }
                else if (TBInput.GetButton(TBInput.Button.SecondaryTrigger, controller)) {
                    if (_teleportationRing != null)
                    {
                        _teleportationRing.transform.position = hitPos;
                    } 
                    else
                    {
                        _teleportationRing = Instantiate(teleportationRingPrefab, hitPos, Quaternion.identity);
                    }
                }
                else
                {
                    Destroy(_teleportationRing);
                    _teleportationRing = null;
                }
            }
            else
            {
                Destroy(_teleportationRing);
                _teleportationRing = null;
            }
        } 
        else
        {
            Destroy(_teleportationRing);
            _teleportationRing = null;
        }


        if (TBInput.GetButton(TBInput.Button.SecondaryTrigger, controller))
        {
            lineRenderer.positionCount = 2;

            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, transform.position + rayDir * 10);
        }
        else
        {
            lineRenderer.positionCount = 0;
        }
    }
}
