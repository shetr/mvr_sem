﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TButt;

public class RotateController : MonoBehaviour
{
    [SerializeField] private TBInput.Controller controller;
    [SerializeField] private float rotateStep = 30;

    private bool turned = false;

    private bool moved = false;
    private float movementSpeed = 1.5f;

    private float rotateThreshold = 0.5f;
    private float moveThreshold = 0.4f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 pos = TBInput.GetAxis2D(TBInput.Button.Joystick, controller);

        if(!turned && Mathf.Abs(pos.x) > rotateThreshold)
        {
            if (pos.x < 0) 
            {
                TBCameraRig.instance.transform.rotation *= Quaternion.Euler(0, -rotateStep, 0);
            } 
            else
            {
                TBCameraRig.instance.transform.rotation *= Quaternion.Euler(0, rotateStep, 0);
            }
            turned = true;
        }
        else if(turned && Mathf.Abs(pos.x) < rotateThreshold)
        {
            turned = false;
        }



        if (Mathf.Abs(pos.y) > moveThreshold)
        {
            //for slower walking backwards
            if(pos.y < 0)
            {
                pos.y = (pos.y + moveThreshold) / (1 - moveThreshold);
                pos.y /= 2;
            }
            else
            {
                pos.y = (pos.y - moveThreshold) / (1 - moveThreshold);
            }

            Vector3 tmpPos = TBCameraRig.instance.transform.position;

            tmpPos.x = TBCameraRig.instance.transform.position.x + TBCameraRig.instance.GetCenterEyeCamera().transform.forward.x * Time.deltaTime * movementSpeed * pos.y;
            tmpPos.z = TBCameraRig.instance.transform.position.z + TBCameraRig.instance.GetCenterEyeCamera().transform.forward.z * Time.deltaTime * movementSpeed * pos.y;
            tmpPos.y = TBCameraRig.instance.transform.position.y;

            TBCameraRig.instance.transform.position = tmpPos;
        }
    }
}
