# MVR projects

This is copied part of repository <https://gitlab.fel.cvut.cz/b201_b0b39mvr/team01> made within subject MVR (modeling and virtual reality) at CTU FEE.
It contains one VR project made in team and two other projects made on my own: AR project and 3D reconstruction project.
More information can be found at <https://shetr.github.io/mvr_web/>


