﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class TrackableManager : MonoBehaviour
{

    [SerializeField]
    private GameObject spiderPrefab;
    [SerializeField]
    private Text infoText;

    private ARTrackedImageManager _imageManager;

    private Dictionary<TrackableId, GameObject> _instantiatedObjects = new Dictionary<TrackableId, GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        _imageManager = FindObjectOfType<ARTrackedImageManager>();
        _imageManager.trackedImagesChanged += _imageManager_trackedImagesChanged;
        infoText.text = "Najdi pavouka";
    }
    // Update is called once per frame
    void Update()
    {

    }

    private void _imageManager_trackedImagesChanged(ARTrackedImagesChangedEventArgs args)
    {
        foreach(ARTrackedImage image in args.added)
        {
            GameObject instantiatedGO = Instantiate(spiderPrefab, image.transform.position, image.transform.rotation);
            _instantiatedObjects.Add(image.trackableId, instantiatedGO);
            infoText.text = "";
        }
        foreach(ARTrackedImage image in args.removed)
        {
            Destroy(_instantiatedObjects[image.trackableId]);
            _instantiatedObjects.Remove(image.trackableId);
        }
        foreach (ARTrackedImage image in args.updated)
        {
            _instantiatedObjects[image.trackableId].transform.position = image.transform.position;
            _instantiatedObjects[image.trackableId].transform.rotation = image.transform.rotation;
        }
        if(_instantiatedObjects.Count == 0)
        {
            infoText.text = "Najdi pavouka";
        }
    }

}
