﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderLegManager : MonoBehaviour
{
    [SerializeField]
    private SpiderLeg legLeft0;
    [SerializeField]
    private SpiderLeg legLeft1;
    [SerializeField]
    private SpiderLeg legLeft2;
    [SerializeField]
    private SpiderLeg legLeft3;
    [SerializeField]
    private SpiderLeg legRight0;
    [SerializeField]
    private SpiderLeg legRight1;
    [SerializeField]
    private SpiderLeg legRight2;
    [SerializeField]
    private SpiderLeg legRight3;

    private SpiderLeg[] _legs;

    // Start is called before the first frame update
    void Start()
    {
        _legs = new SpiderLeg[8];
        _legs[0] = legLeft0;
        _legs[1] = legLeft1;
        _legs[2] = legLeft2;
        _legs[3] = legLeft3;
        _legs[4] = legRight0;
        _legs[5] = legRight1;
        _legs[6] = legRight2;
        _legs[7] = legRight3;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CanMakeStep(SpiderLeg leg)
    {
        int legID = GetSpiderLegID(leg);
        bool canMakeStep = true;
        if (legID < 4)
        {
            canMakeStep = canMakeStep && !_legs[legID+4].IsMakingStep();
        }
        else
        {
            canMakeStep = canMakeStep && !_legs[legID - 4].IsMakingStep();
        }
        int legID4 = legID % 4;
        if (legID4 > 0)
        {
            canMakeStep = canMakeStep && !_legs[legID - 1].IsMakingStep();
        }
        if (legID4 < 3)
        {
            canMakeStep = canMakeStep && !_legs[legID + 1].IsMakingStep();
        }

        return canMakeStep;
    }

    int GetSpiderLegID(SpiderLeg leg)
    {
        int instanceID = leg.GetInstanceID();
        if(instanceID == legLeft0.GetInstanceID())
        {
            return 0;
        }
        else if (instanceID == legLeft1.GetInstanceID())
        {
            return 1;
        }
        else if (instanceID == legLeft2.GetInstanceID())
        {
            return 2;
        }
        else if (instanceID == legLeft3.GetInstanceID())
        {
            return 3;
        }
        else if (instanceID == legRight0.GetInstanceID())
        {
            return 4;
        }
        else if (instanceID == legRight1.GetInstanceID())
        {
            return 5;
        }
        else if (instanceID == legRight2.GetInstanceID())
        {
            return 6;
        }
        else if (instanceID == legRight3.GetInstanceID())
        {
            return 7;
        }
        return 0;
    }
}
