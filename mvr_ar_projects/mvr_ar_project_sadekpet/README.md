# SpiderAR

## Petr Šádek : sadekpet@fel.cvut.cz

Tato AR aplikace zobrazí nad obrázek s pavoukem model animovaného pavouka. Animace je procedurální, pavouk hýbe s nohami podle toho jak se mění jeho pozice.

Buildnutý apk: SpiderAR.apk

Demonstrační video: demonstration.mp4

Použitý obrázek pavouka - spider_mark.png vytisknut zhruba na 7x7 cm kus papíru.

Ve složce spider_progress jsou pro zajímavost videa z postupu práce na pavoukovi.